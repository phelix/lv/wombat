# Wavefront optics measurement and beam analysis tool
![alt text](Logos/WombatLogo.svg "WOMBAT Logo")

This LabVIEW project contains the WOMBAT application. It's a modular software for the acquisition of image data (both real and simulated cameras available) as well as image data analysis. Its main focus is the analysis of images delivered by a Shack-Hartmann sensor for wavefront measurement.

**Cloning the software**

You can clone this repository and its submodule with the following command
```
    git clone https://git.gsi.de/phelix/lv/wombat.git
    git submodule init
    git submodule update
```
