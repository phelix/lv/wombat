﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Description" Type="Str">Copyright 2023 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Released under GPLv3.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="NF Settings" Type="Folder">
		<Item Name="Messages" Type="Folder">
			<Item Name="OnApplyChanges Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/OnApplyChanges Msg/OnApplyChanges Msg.lvclass"/>
			<Item Name="OnEllipseFlipChange Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/OnEllipseFlipChange Msg/OnEllipseFlipChange Msg.lvclass"/>
			<Item Name="OnNewImagePath Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/OnNewImagePath Msg/OnNewImagePath Msg.lvclass"/>
			<Item Name="OnNFPaddingRatioChange Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/OnNFPaddingRatioChange Msg/OnNFPaddingRatioChange Msg.lvclass"/>
			<Item Name="OnOffsetChange Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/OnOffsetChange Msg/OnOffsetChange Msg.lvclass"/>
			<Item Name="OnRadiusChange Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/OnRadiusChange Msg/OnRadiusChange Msg.lvclass"/>
			<Item Name="OnResolutionChange Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/OnResolutionChange Msg/OnResolutionChange Msg.lvclass"/>
			<Item Name="OnRotationChange Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/OnRotationChange Msg/OnRotationChange Msg.lvclass"/>
			<Item Name="OnUseImageChange Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/OnUseImageChange Msg/OnUseImageChange Msg.lvclass"/>
			<Item Name="RegenerateNFPreview Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/RegenerateNFPreview Msg/RegenerateNFPreview Msg.lvclass"/>
			<Item Name="UnhideFP Msg.lvclass" Type="LVClass" URL="../NFSettingsActor Messages/UnhideFP Msg/UnhideFP Msg.lvclass"/>
		</Item>
		<Item Name="NFSettingsActor.lvclass" Type="LVClass" URL="../NFSettingsActor/NFSettingsActor.lvclass"/>
	</Item>
	<Item Name="Messages" Type="Folder">
		<Item Name="Apply NF Generation Parameters Msg.lvclass" Type="LVClass" URL="../../OAMFocusAnalysisActor Messages/Apply NF Generation Parameters Msg/Apply NF Generation Parameters Msg.lvclass"/>
		<Item Name="SetDonutAnalysisParameters Msg.lvclass" Type="LVClass" URL="../../OAMFocusAnalysisActor Messages/SetDonutAnalysisParameters Msg/SetDonutAnalysisParameters Msg.lvclass"/>
		<Item Name="SetPalette Msg.lvclass" Type="LVClass" URL="../../OAMFocusAnalysisActor Messages/SetPalette Msg/SetPalette Msg.lvclass"/>
		<Item Name="Write Overlay options Msg.lvclass" Type="LVClass" URL="../../OAMFocusAnalysisActor Messages/Write Overlay options Msg/Write Overlay options Msg.lvclass"/>
		<Item Name="ShowNFSettigns Msg.lvclass" Type="LVClass" URL="../../OAMFocusAnalysisActor Messages/ShowNFSettigns Msg/ShowNFSettigns Msg.lvclass"/>
		<Item Name="Write Norm. Cam. Img._ Msg.lvclass" Type="LVClass" URL="../../OAMFocusAnalysisActor Messages/Write Norm. Cam. Img._ Msg/Write Norm. Cam. Img._ Msg.lvclass"/>
	</Item>
	<Item Name="OAMFocusAnalysisActor.lvclass" Type="LVClass" URL="../OAMFocusAnalysisActor.lvclass"/>
</Library>
